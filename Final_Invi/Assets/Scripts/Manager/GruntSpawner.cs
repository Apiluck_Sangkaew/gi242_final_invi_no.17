﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class GruntSpawner : MonoBehaviour
{
   public GameObject Grunt_01;

   //private float Timer = 0.0f;
   private float SpawnTimer = 2f;

   private void Start()
   {
      Invoke("SpawnGrunt", SpawnTimer);
         //InvokeRepeating("");
   }

   private void SpawnGrunt()
   {
      Vector2 min = Camera.main.ViewportToWorldPoint(new Vector2(0, 0));
      
      Vector2 max = Camera.main.ViewportToWorldPoint(new Vector2(1, 1));

      GameObject anEnemy = (GameObject) Instantiate(Grunt_01);
      anEnemy.transform.position = new Vector2(Random.Range(min.x, max.x), max.y);
      
      NextGruntSpawn();
   }

   void NextGruntSpawn()
   {
      float timer;

      if (SpawnTimer > 1f)
      {
         timer = Random.Range(1f, SpawnTimer);
      }
      else
         timer = 1f;
      
      Invoke("SpawnGrunt", timer);
   }
}
