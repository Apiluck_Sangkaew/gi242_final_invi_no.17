﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Manager
{
    public class ButtonManager : MonoBehaviour
    {
        [SerializeField] private Button boss1Button;
        [SerializeField] private Button boss2Button;
        [SerializeField] private Button exitButton;

        private void Awake()
        {
            boss1Button.onClick.AddListener(OnBoss1ButtonClicked);
            boss2Button.onClick.AddListener(OnBoss2ButtonClicked);
            exitButton.onClick.AddListener(OnExitButtonClicked);
        }
        
        private void OnBoss1ButtonClicked()
        {
            SceneManager.LoadScene("Game");
        }
        private void OnBoss2ButtonClicked()
        {
            SceneManager.LoadScene("Game2");
        }
        private void OnExitButtonClicked()
        {
            Application.Quit();
        }

    }
}
