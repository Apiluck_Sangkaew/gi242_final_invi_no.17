﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Manager
{
    public class ExitManager : MonoBehaviour
    {
        [SerializeField] private Button exitButton;

        private void Awake()
        {
            exitButton.onClick.AddListener(ExitButtonClicked);
            
        }

        private void ExitButtonClicked()
        {
            Application.Quit();
        }
    }
}
